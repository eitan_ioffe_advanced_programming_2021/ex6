#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "inputExceptions.h"
#include "strInputException.h"
#include "Pentagon.h"
#include "Hexagon.h"

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180, side = 0; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);

	Pentagon* pen = new Pentagon(nam, col, side); // adding the 2 new shapes
	Hexagon* hex = new Hexagon(nam, col, side);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p';
	std::string shapetype = "";
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, e = pentagon, h = hexagon" << std::endl;
	try {
		std::getline(std::cin, shapetype);
		if (shapetype.length() > 1) {
			throw StrInputException();
		}

			switch (shapetype[0]) {
			case 'c':
			{
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;

				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
			};
			break;
			case 'q':
			{
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
			}; break;
			case 'r':
			{
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
			};
			break;
			case 'p':
			{
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			};
			break;
			case 'e': // for pentagon
			{
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> side;
				pen->setName(nam);
				pen->setColor(col);
				pen->setSide(side);
				pen->draw();
			}; 
			break;
			case 'h': // for hexagon
			{
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> side;
				hex->setName(nam);
				hex->setColor(col);
				hex->setSide(side);
				hex->draw();
			};
			
			default:
			{
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
			};
			break;

			}

			if (std::cin.fail())
			{
				throw inputException();
			}

			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
			getchar(); // cleaning buffer
		}
		
		catch (const StrInputException& e) // if user input is more than 1 character 
		{
			std::cerr << e.what() << std::endl;
			continue;
		}
		catch (const std::string &e) // if the number is negative
		{			
			std::cerr << e << std::endl;
		}
		catch (const inputException::exception &e) // if user input is not a number
		{
			std::cerr << e.what() << std::endl;
			std::cin.clear();

			// discard 'bad' characters
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}