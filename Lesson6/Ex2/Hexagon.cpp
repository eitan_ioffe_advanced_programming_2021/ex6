#include "Hexagon.h"

Hexagon::Hexagon(std::string name, std::string color, double n) : Shape(name, color)
{
	this->_side = n;
}

Hexagon::~Hexagon()
{
	this->_side = 0;
}

void Hexagon::setSide(double n)
{
	this->_side = n;
}

void Hexagon::draw()
{
	std::cout << "name: " << this->getName() << std::endl;
	std::cout << "color: " << this->getColor() << std::endl;
	std::cout << "Side size: " << this->_side << std::endl;
	std::cout << "Area: " << this->CalcArea() << std::endl;
}

double Hexagon::CalcArea()
{
	return MathUtils::CalHexagonArea(this->_side);
}
