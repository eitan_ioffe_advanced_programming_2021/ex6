#pragma once
#include <iostream>
#include "shape.h"
#include "MathUtils.h"

class Pentagon : public Shape
{
public:
	Pentagon(std::string name, std::string color, double n);
	~Pentagon();

	void setSide(double n);

	virtual void draw(); // draw is the only abstract method of shape
	double CalcArea();

private:
	double _side;
};
