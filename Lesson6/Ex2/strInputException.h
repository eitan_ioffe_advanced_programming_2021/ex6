#pragma once
#include <exception>
#include <iostream>

class StrInputException : public std::exception
{
public:
	virtual const char* what() const;
};

const char* StrInputException::what() const
{
	return "Warning - Don't try to build more than one shape at once";
}
