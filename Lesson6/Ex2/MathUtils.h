#pragma once
#include <iostream>
#include <math.h>

class MathUtils
{
public:
	static double CalPentagonArea(double n);
	static double CalHexagonArea(double n);

};
