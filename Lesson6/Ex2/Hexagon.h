#pragma once
#include <iostream>
#include "shape.h"
#include "MathUtils.h"

class Hexagon : public Shape
{
public:
	Hexagon(std::string name, std::string color, double n);
	~Hexagon();

	void setSide(double n);

	virtual void draw(); // draw is the only abstract method of shape
	double CalcArea();

private:
	double _side;
};
