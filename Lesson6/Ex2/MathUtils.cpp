#include "MathUtils.h"

double MathUtils::CalPentagonArea(double n)
{
	double area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * n * n) / 4;
	return area;
}

double MathUtils::CalHexagonArea(double n)
{
	double area = ((3 * sqrt(3) * (n * n)) / 2);
	return area;
}
