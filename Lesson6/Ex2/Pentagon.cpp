#include "Pentagon.h"
#include "shapeexception.h"

Pentagon::Pentagon(std::string name, std::string color, double n) : Shape(name, color)
{
	this->_side = n;
}

Pentagon::~Pentagon()
{
	this->_side = 0;
}

void Pentagon::setSide(double n)
{
	this->_side = n;
	if (_side < 0)
	{
		throw shapeException();
	}
}

void Pentagon::draw()
{
	std::cout << "name: " << this->getName() << std::endl;
	std::cout << "color: " << this->getColor() << std::endl;
	std::cout << "Side size: " << this->_side << std::endl;
	std::cout << "Area: " << this->CalcArea() << std::endl;
}

double Pentagon::CalcArea()
{
	return MathUtils::CalPentagonArea(this->_side);
}
