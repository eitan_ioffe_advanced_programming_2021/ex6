#pragma once
#include <exception>
#include <iostream>

class inputException : public std::exception
{
public:
	virtual const char* what() const;
};

const char* inputException::what() const
{
	return "ERROR: Input is not good";
}