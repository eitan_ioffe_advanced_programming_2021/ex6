#include <iostream>

#define FORBIDDEN_NUM 8200
#define ERROR_CODE -1
#define ERROR_MSG "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"

int add(int a, int b, bool& isForbidden) {
    if (a + b == FORBIDDEN_NUM && !isForbidden)
    {
        isForbidden = true;
        return ERROR_CODE;
    }
    return a + b;
}

int  multiply(int a, int b, bool& isForbidden) {
    int sum = 0;
    for (int i = 0; i < b; i++) {
        sum = add(sum, a, isForbidden);
        if (isForbidden) {
            return ERROR_CODE;
        }
    };
    return sum;
}

int  pow(int a, int b, bool& isForbidden) {
    int exponent = 1;
    for (int i = 0; i < b; i++) {
        exponent = multiply(exponent, a, isForbidden);
        if (isForbidden) {
            return ERROR_CODE;
        }
    };
    return exponent;
}

int main(void) {
    bool isForbidden = false;

    std::cout << add(8200, 2, isForbidden) << std::endl;
    if (isForbidden)
    {
        std::cerr << ERROR_MSG << std::endl;
        isForbidden = false;
    }

    std::cout << add(8198, 2, isForbidden) << std::endl;
    if (isForbidden)
    {
        std::cerr << ERROR_MSG << std::endl;
        isForbidden = false;
    }

    std::cout << multiply(8200, 3, isForbidden) << std::endl;
    if (isForbidden)
    {
        std::cerr << ERROR_MSG << std::endl;
        isForbidden = false;
    }

    std::cout << multiply(8199, 1, isForbidden) << std::endl;
    if (isForbidden)
    {
        std::cerr << ERROR_MSG << std::endl;
        isForbidden = false;
    }

    std::cout << pow(5, 5, isForbidden) << std::endl;
    if (isForbidden)
    {
        std::cerr << ERROR_MSG << std::endl;
        isForbidden = false;
    }
}