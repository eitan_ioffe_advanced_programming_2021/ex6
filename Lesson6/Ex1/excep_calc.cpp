#include <iostream>

#define FORBIDDEN_NUM 8200

int add(int a, int b) {
    if (a + b == FORBIDDEN_NUM)
    {
        throw (std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"));
    }
    return a + b;
}

int  multiply(int a, int b) {
    int sum = 0;
    for (int i = 0; i < b; i++) {
        sum = add(sum, a);
    };
    return sum;
}

int  pow(int a, int b) {
    int exponent = 1;
    for (int i = 0; i < b; i++) {
        exponent = multiply(exponent, a);
    };
    return exponent;
}

int main(void) {
    try
    {
        std::cout << add(8200, 2) << std::endl; // legal
    }
    catch (const std::string& errorString)
    {
        std::cerr << "ERROR: " << errorString << std::endl;
    }

    try
    {
        std::cout << add(8198, 2) << std::endl; // cals 8200 - illegal
    }
    catch (const std::string& errorString)
    {
        std::cerr << "ERROR: " << errorString << std::endl;
    }

    try
    {
        std::cout << multiply(8200, 3) << std::endl; // exponent will contain 8200 - error
    }
    catch (const std::string& errorString)
    {
        std::cerr << "ERROR: " << errorString << std::endl;
    }

    try
    {
        std::cout << multiply(8199, 1) << std::endl; // legal
    }
    catch (const std::string& errorString)
    {
        std::cerr << "ERROR: " << errorString << std::endl;
    }

    try
    {
        std::cout << pow(5, 5) << std::endl; // legal
    }
    catch (const std::string& errorString)
    {
        std::cerr << "ERROR: " << errorString << std::endl;
    }
}
